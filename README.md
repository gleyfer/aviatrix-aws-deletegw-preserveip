# aviatrix-aws-deletegw-preserveip

Script to delete an Aviatrix gateway, which was created with 'allocate EIP' checkbox checked in AWS, and immediately reclaim the EIP.

Reference: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html#using-eip-recovering](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html#using-eip-recovering)

**Note:** Although the reclaim operation runs immediately after the gateway deletion, this approach is best-effort and there is no guarantee that the EIP will be reclaimed.

Example Output:
```
cloudshell-user@ip-10-0-126-220 aviatrix-aws-deletegw-preserveip]$ ./deletegw.sh
Aviatrix Controller IP or hostname: 52.8.95.195
Aviatrix Controller User: admin
Aviatrix Controller Password: 
Aviatrix Gateway to delete: egress-hagw
{"return":true,"results":"Gateway egress-hagw has been deleted."}
    "PublicIp": "52.38.72.42",
    "AllocationId": "eipalloc-030b9fede49fd68c7",
    "PublicIpv4Pool": "amazon",
    "NetworkBorderGroup": "us-west-2",
    "Domain": "vpc"
}
```
