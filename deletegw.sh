#!/bin/bash

#Get necessary info
read -p 'Aviatrix Controller IP or hostname: ' ControllerIP
read -p 'Aviatrix Controller User: ' ControllerUser
read -sp 'Aviatrix Controller Password: ' ControllerPW
echo
read -p 'Aviatrix Gateway to delete: ' GWName

if [[ (-z $ControllerIP || -z $ControllerPW || -z $GWName || -z $ControllerUser) ]]
then
	echo "Please specify all of the necessary information to continue"
	exit 1
fi

#Get CID
CID=`curl -s -k --location --request POST "https://$ControllerIP/v1/api" \
--form 'action=login' \
--form 'username=admin' \
--form "password=$ControllerPW" | grep -o 'CID.*'|cut -d'"' -f3`

#Get gateway public EIP
gwPublicIP=`curl -s -k --location --request GET "https://$ControllerIP/v1/api?action=get_gateway_info&CID=$CID&gateway_name=$GWName"|jq -r .results.public_ip`

#Delete the gateway specified
curl -s -k --location --request POST "https://$ControllerIP/v1/api" \
--form 'action=delete_container' \
--form "CID=$CID" \
--form 'cloud_type=1' \
--form "gw_name=$GWName"

#Reclaim the EIP
aws ec2 allocate-address --domain vpc --address $gwPublicIP --tag-specifications "ResourceType=elastic-ip,Tags=[{Key=Name,Value=Aviatrix-eip@${GWName}-${gwPublicIP}}]"
